# flexibee-unlocker
## Script pro hromadné odemykání/zamykání dokladů ve FlexiBee.
Nástroj po spuštění provede odemčení nebo zamčení vybraných dokladů v účetnictví ABRA FlexiBee.

## Instalace a použití
Do FlexiBee naimportujte uživatelské tlačítko, které je součástí repozitáře. Import provedete pomocí horního menu *Nástroje - Import - Import z XML*.
Po úspěšném importu budou v evidenci Vydané faktury (horní menu *Prodej - Vydané faktury*) nebo Přijaté faktury (horní menu *Nákup - Přijaté faktury*) nebo Banka (horní menu *Peníze - Banka*)
dostupná nová tlačítka Odemknout a Zamknout.

Zaškrtejte doklady, které chcete odemknout nebo zamknout a klikněte na tlačítko Odemknout nebo Zamknout.

Spustí se script, který po svém úspěšném dokončení vypíše kolik bylo odemčeno nebo zamčeno dokladů. Pokud dojde k chybě, bude vypsáno chybové hlášení a žádný doklad nebude odemčen nebo zamčen.
Opravte chybu a spusťte proces znovu.

## Systémové a licenční požadavky
* **REST-API pro zápis**. Pro správnou funkčnost uživatelského tlačítka je nutné mít některou z placených licencí FlexiBee. Tyto licence totiž obsahují REST-API s možností zápisu.

## Parametry volání scriptu
**url** - url encodovaná adresa flexibee serveru s idenitifkátorem firmy a evidencí (např. https://demo.flexibee.eu/c/demo/banka ).

**authSessionId** - authSessionId právě přihlášeného uživatele z FlexiBee. Odlášením uživatele z aplikace se zneplatní.

**ids** - seznam identifikátorů bankovních pohybů, které se mají odpárovat. Oddělené čárkou (např. 1,2,3). Povoleny jsou pouze interní identifikátory FlexiBee. 

**evidence** - identifikátor evidence ve FlexiBee. Budou v ní vyhledávány doklady k odemčení (např. *faktura-vydana*, *faktura-prijata* nebo *banka*).

**action** - lock|lock-for-ucetni|unlock; akce která se má vykonat, pokud není uvedeno tak bude odemykání (unlock)

## Zdroje
* https://charlieblog.eu/clanek-flexibee-uzivatelska-tlacitka
* https://www.flexibee.eu/api/dokumentace/ref/uzivatelske-tlacitko/
* https://www.flexibee.eu/api/dokumentace/ref/zamykani-odemykani/
* https://www.flexibee.eu/api/dokumentace/ref/login/
 

Foto: https://unsplash.com/photos/bBavss4ZQcA